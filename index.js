var Hose = require('flowr-hose')
var cors = require('cors')

var hose = new Hose({
  name: 'webhook',
  port: process.env.PORT || 3000,
  kue: {
    prefix: 'q',
    redis: process.env.KUE
  }
});

hose.http.app.use(cors())

hose.create('gardener:active-inputs', {
  input: 'webhook'
})
.then(function (job) {
  return job.promise
})
.then(function (data) {
  data.map(function (config) {
    hose.http.app[config.method]('/' + config.path, function(req, res) {
      hose.create('gardener:receive', {
        data: {
          body: req.body,
          params: req.params
        },
        flow: config._flow,
        block: config._block
      })
      .then(function (job) {
        if (!config.wait_flow_result) {
          res.json(job.data._fToken)
        } else {
          connections[job.data._fToken] = res
        }
      })
      .catch(function (err) {
        console.error(err)
        res.sendStatus(500)
      })
    })
  })
})

var connections = {}

hose.process('deactivate', process.env.CONCURRENCY || 1, function(job, done) {
  done()
  var config = job.data
  console.log("DEACTIVATE", config)
  var routes = hose.http.app._router.stack;
  routes.forEach(removeMiddlewares);
  function removeMiddlewares(route, i, routes) {
    if(route.route) {
      if (route.route.path === '/' + config.path) {
        routes.splice(i, 1);
      }
    }
  }
})
hose.process('output', process.env.CONCURRENCY || 1, function (job, done) {
  if (connections[job.data._fToken]) {
    connections[job.data._fToken].type(job.data.type || 'json').send(job.data.input)
    delete connections[job.data._fToken]
    done()
  } else {
    console.log("connection not found")
    done(null, "connection not found")
  }
})

hose.process('activate', process.env.CONCURRENCY || 1, function(job, done) {
  done()
  var config = job.data
  console.log("ACTIVATE", config)
  hose.http.app[config.method]('/' + config.path, function(req, res) {
    hose.create('gardener:receive', {
      data: {
        body: req.body,
        params: req.params
      },
      flow: config._flow,
      block: config._block
    })
    .then(function (job) {
      if (!config.wait_flow_result) {
        res.json(job.data._fToken)
      } else {
        connections[job.data._fToken] = res
      }
    })
    .catch(function (err) {
      console.error(err)
      res.sendStatus(500)
    })
  })
})
